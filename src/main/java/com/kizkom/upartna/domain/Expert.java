package com.kizkom.upartna.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Expert.
 */
@Document(collection = "expert")
public class Expert implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("title")
    private String title;

    @Field("about")
    private String about;

    @Field("website")
    private String website;

    @Field("is_activated")
    private Boolean isActivated;

    @Field("is_company")
    private Boolean isCompany;

    @Field("tax_payer_number")
    private String taxPayerNumber;

    @Field("experience_start_date")
    private LocalDate experienceStartDate;

    @Field("activated_on")
    private LocalDate activatedOn;

    @Field("created_on")
    private LocalDate createdOn;

    @DBRef
    @NotNull
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Expert title(String title) {
        this.title = title;
        return this;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Expert about(String about) {
        this.about = about;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Expert website(String website) {
        this.website = website;
        return this;
    }

    public Boolean isIsActivated() {
        return isActivated;
    }

    public Expert isActivated(Boolean isActivated) {
        this.isActivated = isActivated;
        return this;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean isIsCompany() {
        return isCompany;
    }

    public Expert isCompany(Boolean isCompany) {
        this.isCompany = isCompany;
        return this;
    }

    public void setIsCompany(Boolean isCompany) {
        this.isCompany = isCompany;
    }

    public String getTaxPayerNumber() {
        return taxPayerNumber;
    }

    public void setTaxPayerNumber(String taxPayerNumber) {
        this.taxPayerNumber = taxPayerNumber;
    }

    public Expert taxPayerNumber(String taxPayerNumber) {
        this.taxPayerNumber = taxPayerNumber;
        return this;
    }

    public LocalDate getExperienceStartDate() {
        return experienceStartDate;
    }

    public void setExperienceStartDate(LocalDate experienceStartDate) {
        this.experienceStartDate = experienceStartDate;
    }

    public Expert experienceStartDate(LocalDate experienceStartDate) {
        this.experienceStartDate = experienceStartDate;
        return this;
    }

    public LocalDate getActivatedOn() {
        return activatedOn;
    }

    public void setActivatedOn(LocalDate activatedOn) {
        this.activatedOn = activatedOn;
    }

    public Expert activatedOn(LocalDate activatedOn) {
        this.activatedOn = activatedOn;
        return this;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public Expert createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Expert expert = (Expert) o;
        if (expert.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), expert.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Expert{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", about='" + getAbout() + "'" +
            ", website='" + getWebsite() + "'" +
            ", isActivated='" + isIsActivated() + "'" +
            ", isCompany='" + isIsCompany() + "'" +
            ", taxPayerNumber='" + getTaxPayerNumber() + "'" +
            ", experienceStartDate='" + getExperienceStartDate() + "'" +
            ", activatedOn='" + getActivatedOn() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            "}";
    }
}
