package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.Domain;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Domain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DomainRepository extends MongoRepository<Domain, String> {

}
