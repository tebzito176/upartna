package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.Realization;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Realization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RealizationRepository extends MongoRepository<Realization, String> {

}
