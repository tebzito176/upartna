package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.JobFileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing JobFile.
 */
public interface JobFileService {

    /**
     * Save a jobFile.
     *
     * @param jobFileDTO the entity to save
     * @return the persisted entity
     */
    JobFileDTO save(JobFileDTO jobFileDTO);

    /**
     * Get all the jobFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<JobFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" jobFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    JobFileDTO findOne(String id);

    /**
     * Delete the "id" jobFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
