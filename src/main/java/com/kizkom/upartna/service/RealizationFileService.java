package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.RealizationFileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing RealizationFile.
 */
public interface RealizationFileService {

    /**
     * Save a realizationFile.
     *
     * @param realizationFileDTO the entity to save
     * @return the persisted entity
     */
    RealizationFileDTO save(RealizationFileDTO realizationFileDTO);

    /**
     * Get all the realizationFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RealizationFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" realizationFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    RealizationFileDTO findOne(String id);

    /**
     * Delete the "id" realizationFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
