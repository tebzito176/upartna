package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.RealizationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Realization.
 */
public interface RealizationService {

    /**
     * Save a realization.
     *
     * @param realizationDTO the entity to save
     * @return the persisted entity
     */
    RealizationDTO save(RealizationDTO realizationDTO);

    /**
     * Get all the realizations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RealizationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" realization.
     *
     * @param id the id of the entity
     * @return the entity
     */
    RealizationDTO findOne(String id);

    /**
     * Delete the "id" realization.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
