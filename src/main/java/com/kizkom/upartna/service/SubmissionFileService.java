package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.SubmissionFileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SubmissionFile.
 */
public interface SubmissionFileService {

    /**
     * Save a submissionFile.
     *
     * @param submissionFileDTO the entity to save
     * @return the persisted entity
     */
    SubmissionFileDTO save(SubmissionFileDTO submissionFileDTO);

    /**
     * Get all the submissionFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubmissionFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" submissionFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubmissionFileDTO findOne(String id);

    /**
     * Delete the "id" submissionFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
