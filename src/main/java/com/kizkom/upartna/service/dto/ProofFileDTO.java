package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Proof;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ProofFile entity.
 */
public class ProofFileDTO implements Serializable {

    private String id;

    private String url;

    private String fileType;

    private String extension;

    @NotNull
    private Proof proof;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Proof getProof() {
        return proof;
    }

    public void setProof(Proof proof) {
        this.proof = proof;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProofFileDTO proofFileDTO = (ProofFileDTO) o;
        if (proofFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), proofFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProofFileDTO{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", fileType='" + getFileType() + "'" +
            ", extension='" + getExtension() + "'" +
            "}";
    }
}
