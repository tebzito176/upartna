package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.JobFileService;
import com.kizkom.upartna.domain.JobFile;
import com.kizkom.upartna.repository.JobFileRepository;
import com.kizkom.upartna.service.dto.JobFileDTO;
import com.kizkom.upartna.service.mapper.JobFileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing JobFile.
 */
@Service
public class JobFileServiceImpl implements JobFileService {

    private final Logger log = LoggerFactory.getLogger(JobFileServiceImpl.class);

    private final JobFileRepository jobFileRepository;

    private final JobFileMapper jobFileMapper;

    public JobFileServiceImpl(JobFileRepository jobFileRepository, JobFileMapper jobFileMapper) {
        this.jobFileRepository = jobFileRepository;
        this.jobFileMapper = jobFileMapper;
    }

    /**
     * Save a jobFile.
     *
     * @param jobFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public JobFileDTO save(JobFileDTO jobFileDTO) {
        log.debug("Request to save JobFile : {}", jobFileDTO);
        JobFile jobFile = jobFileMapper.toEntity(jobFileDTO);
        jobFile = jobFileRepository.save(jobFile);
        return jobFileMapper.toDto(jobFile);
    }

    /**
     * Get all the jobFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<JobFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JobFiles");
        return jobFileRepository.findAll(pageable)
            .map(jobFileMapper::toDto);
    }

    /**
     * Get one jobFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public JobFileDTO findOne(String id) {
        log.debug("Request to get JobFile : {}", id);
        JobFile jobFile = jobFileRepository.findOne(id);
        return jobFileMapper.toDto(jobFile);
    }

    /**
     * Delete the jobFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete JobFile : {}", id);
        jobFileRepository.delete(id);
    }
}
