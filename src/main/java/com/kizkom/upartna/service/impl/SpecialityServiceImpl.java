package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.SpecialityService;
import com.kizkom.upartna.domain.Speciality;
import com.kizkom.upartna.repository.SpecialityRepository;
import com.kizkom.upartna.service.dto.SpecialityDTO;
import com.kizkom.upartna.service.mapper.SpecialityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Speciality.
 */
@Service
public class SpecialityServiceImpl implements SpecialityService {

    private final Logger log = LoggerFactory.getLogger(SpecialityServiceImpl.class);

    private final SpecialityRepository specialityRepository;

    private final SpecialityMapper specialityMapper;

    public SpecialityServiceImpl(SpecialityRepository specialityRepository, SpecialityMapper specialityMapper) {
        this.specialityRepository = specialityRepository;
        this.specialityMapper = specialityMapper;
    }

    /**
     * Save a speciality.
     *
     * @param specialityDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SpecialityDTO save(SpecialityDTO specialityDTO) {
        log.debug("Request to save Speciality : {}", specialityDTO);
        Speciality speciality = specialityMapper.toEntity(specialityDTO);
        speciality = specialityRepository.save(speciality);
        return specialityMapper.toDto(speciality);
    }

    /**
     * Get all the specialities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<SpecialityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specialities");
        return specialityRepository.findAll(pageable)
            .map(specialityMapper::toDto);
    }

    /**
     * Get one speciality by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public SpecialityDTO findOne(String id) {
        log.debug("Request to get Speciality : {}", id);
        Speciality speciality = specialityRepository.findOne(id);
        return specialityMapper.toDto(speciality);
    }

    /**
     * Delete the speciality by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Speciality : {}", id);
        specialityRepository.delete(id);
    }
}
