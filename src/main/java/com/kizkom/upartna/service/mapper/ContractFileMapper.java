package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.ContractFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ContractFile and its DTO ContractFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContractFileMapper extends EntityMapper<ContractFileDTO, ContractFile> {


}
