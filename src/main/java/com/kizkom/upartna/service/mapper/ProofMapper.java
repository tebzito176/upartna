package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.ProofDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Proof and its DTO ProofDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProofMapper extends EntityMapper<ProofDTO, Proof> {


}
