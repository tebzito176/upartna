package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.ProofService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.ProofDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Proof.
 */
@RestController
@RequestMapping("/api")
public class ProofResource {

    private static final String ENTITY_NAME = "proof";
    private final Logger log = LoggerFactory.getLogger(ProofResource.class);
    private final ProofService proofService;

    public ProofResource(ProofService proofService) {
        this.proofService = proofService;
    }

    /**
     * POST  /proofs : Create a new proof.
     *
     * @param proofDTO the proofDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new proofDTO, or with status 400 (Bad Request) if the proof has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/proofs")
    @Timed
    public ResponseEntity<ProofDTO> createProof(@RequestBody ProofDTO proofDTO) throws URISyntaxException {
        log.debug("REST request to save Proof : {}", proofDTO);
        if (proofDTO.getId() != null) {
            throw new BadRequestAlertException("A new proof cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProofDTO result = proofService.save(proofDTO);
        return ResponseEntity.created(new URI("/api/proofs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proofs : Updates an existing proof.
     *
     * @param proofDTO the proofDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated proofDTO,
     * or with status 400 (Bad Request) if the proofDTO is not valid,
     * or with status 500 (Internal Server Error) if the proofDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/proofs")
    @Timed
    public ResponseEntity<ProofDTO> updateProof(@RequestBody ProofDTO proofDTO) throws URISyntaxException {
        log.debug("REST request to update Proof : {}", proofDTO);
        if (proofDTO.getId() == null) {
            return createProof(proofDTO);
        }
        ProofDTO result = proofService.save(proofDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, proofDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proofs : get all the proofs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of proofs in body
     */
    @GetMapping("/proofs")
    @Timed
    public ResponseEntity<List<ProofDTO>> getAllProofs(Pageable pageable) {
        log.debug("REST request to get a page of Proofs");
        Page<ProofDTO> page = proofService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/proofs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /proofs/:id : get the "id" proof.
     *
     * @param id the id of the proofDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the proofDTO, or with status 404 (Not Found)
     */
    @GetMapping("/proofs/{id}")
    @Timed
    public ResponseEntity<ProofDTO> getProof(@PathVariable String id) {
        log.debug("REST request to get Proof : {}", id);
        ProofDTO proofDTO = proofService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(proofDTO));
    }

    /**
     * DELETE  /proofs/:id : delete the "id" proof.
     *
     * @param id the id of the proofDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/proofs/{id}")
    @Timed
    public ResponseEntity<Void> deleteProof(@PathVariable String id) {
        log.debug("REST request to delete Proof : {}", id);
        proofService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
