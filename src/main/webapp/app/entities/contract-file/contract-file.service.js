(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('ContractFile', ContractFile);

    ContractFile.$inject = ['$resource'];

    function ContractFile($resource) {
        var resourceUrl = 'api/contract-files/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    }
})();
