(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('contract-file', {
                parent: 'entity',
                url: '/contract-file?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.contractFile.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/contract-file/contract-files.html',
                        controller: 'ContractFileController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('contractFile');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('contract-file-detail', {
                parent: 'contract-file',
                url: '/contract-file/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.contractFile.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/contract-file/contract-file-detail.html',
                        controller: 'ContractFileDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('contractFile');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ContractFile', function ($stateParams, ContractFile) {
                        return ContractFile.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'contract-file',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('contract-file-detail.edit', {
                parent: 'contract-file-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/contract-file/contract-file-dialog.html',
                        controller: 'ContractFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['ContractFile', function (ContractFile) {
                                return ContractFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('contract-file.new', {
                parent: 'contract-file',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/contract-file/contract-file-dialog.html',
                        controller: 'ContractFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    url: null,
                                    fileType: null,
                                    extension: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('contract-file', null, {reload: 'contract-file'});
                    }, function () {
                        $state.go('contract-file');
                    });
                }]
            })
            .state('contract-file.edit', {
                parent: 'contract-file',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/contract-file/contract-file-dialog.html',
                        controller: 'ContractFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['ContractFile', function (ContractFile) {
                                return ContractFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('contract-file', null, {reload: 'contract-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('contract-file.delete', {
                parent: 'contract-file',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/contract-file/contract-file-delete-dialog.html',
                        controller: 'ContractFileDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['ContractFile', function (ContractFile) {
                                return ContractFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('contract-file', null, {reload: 'contract-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
