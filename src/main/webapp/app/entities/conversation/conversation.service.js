(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Conversation', Conversation);

    Conversation.$inject = ['$resource', 'DateUtils'];

    function Conversation($resource, DateUtils) {
        var resourceUrl = 'api/conversations/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdOn = DateUtils.convertLocalDateFromServer(data.createdOn);
                        data.closedOn = DateUtils.convertLocalDateFromServer(data.closedOn);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    copy.closedOn = DateUtils.convertLocalDateToServer(copy.closedOn);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    copy.closedOn = DateUtils.convertLocalDateToServer(copy.closedOn);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
