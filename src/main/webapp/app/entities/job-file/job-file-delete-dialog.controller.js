(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('JobFileDeleteController', JobFileDeleteController);

    JobFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'JobFile'];

    function JobFileDeleteController($uibModalInstance, entity, JobFile) {
        var vm = this;

        vm.jobFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            JobFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
