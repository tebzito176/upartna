(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('JobFileDialogController', JobFileDialogController);

    JobFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'JobFile'];

    function JobFileDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, JobFile) {
        var vm = this;

        vm.jobFile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.jobFile.id !== null) {
                JobFile.update(vm.jobFile, onSaveSuccess, onSaveError);
            } else {
                JobFile.save(vm.jobFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:jobFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
