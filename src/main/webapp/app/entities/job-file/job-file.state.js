(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('job-file', {
                parent: 'entity',
                url: '/job-file?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.jobFile.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/job-file/job-files.html',
                        controller: 'JobFileController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jobFile');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('job-file-detail', {
                parent: 'job-file',
                url: '/job-file/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.jobFile.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/job-file/job-file-detail.html',
                        controller: 'JobFileDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jobFile');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JobFile', function ($stateParams, JobFile) {
                        return JobFile.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'job-file',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('job-file-detail.edit', {
                parent: 'job-file-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/job-file/job-file-dialog.html',
                        controller: 'JobFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['JobFile', function (JobFile) {
                                return JobFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('job-file.new', {
                parent: 'job-file',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/job-file/job-file-dialog.html',
                        controller: 'JobFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    url: null,
                                    fileType: null,
                                    extension: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('job-file', null, {reload: 'job-file'});
                    }, function () {
                        $state.go('job-file');
                    });
                }]
            })
            .state('job-file.edit', {
                parent: 'job-file',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/job-file/job-file-dialog.html',
                        controller: 'JobFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['JobFile', function (JobFile) {
                                return JobFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('job-file', null, {reload: 'job-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('job-file.delete', {
                parent: 'job-file',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/job-file/job-file-delete-dialog.html',
                        controller: 'JobFileDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['JobFile', function (JobFile) {
                                return JobFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('job-file', null, {reload: 'job-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
