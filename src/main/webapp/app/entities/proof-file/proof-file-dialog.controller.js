(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofFileDialogController', ProofFileDialogController);

    ProofFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProofFile'];

    function ProofFileDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ProofFile) {
        var vm = this;

        vm.proofFile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.proofFile.id !== null) {
                ProofFile.update(vm.proofFile, onSaveSuccess, onSaveError);
            } else {
                ProofFile.save(vm.proofFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:proofFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
