(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofDeleteController', ProofDeleteController);

    ProofDeleteController.$inject = ['$uibModalInstance', 'entity', 'Proof'];

    function ProofDeleteController($uibModalInstance, entity, Proof) {
        var vm = this;

        vm.proof = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            Proof.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
