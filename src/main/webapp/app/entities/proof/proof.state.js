(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('proof', {
                parent: 'entity',
                url: '/proof?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.proof.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/proof/proofs.html',
                        controller: 'ProofController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('proof');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('proof-detail', {
                parent: 'proof',
                url: '/proof/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.proof.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/proof/proof-detail.html',
                        controller: 'ProofDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('proof');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Proof', function ($stateParams, Proof) {
                        return Proof.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'proof',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('proof-detail.edit', {
                parent: 'proof-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof/proof-dialog.html',
                        controller: 'ProofDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Proof', function (Proof) {
                                return Proof.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('proof.new', {
                parent: 'proof',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof/proof-dialog.html',
                        controller: 'ProofDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    description: null,
                                    createdOn: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('proof', null, {reload: 'proof'});
                    }, function () {
                        $state.go('proof');
                    });
                }]
            })
            .state('proof.edit', {
                parent: 'proof',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof/proof-dialog.html',
                        controller: 'ProofDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Proof', function (Proof) {
                                return Proof.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('proof', null, {reload: 'proof'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('proof.delete', {
                parent: 'proof',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof/proof-delete-dialog.html',
                        controller: 'ProofDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Proof', function (Proof) {
                                return Proof.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('proof', null, {reload: 'proof'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
