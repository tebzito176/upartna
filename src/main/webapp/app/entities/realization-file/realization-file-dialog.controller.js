(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationFileDialogController', RealizationFileDialogController);

    RealizationFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RealizationFile'];

    function RealizationFileDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, RealizationFile) {
        var vm = this;

        vm.realizationFile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.realizationFile.id !== null) {
                RealizationFile.update(vm.realizationFile, onSaveSuccess, onSaveError);
            } else {
                RealizationFile.save(vm.realizationFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:realizationFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
