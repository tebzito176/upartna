(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('SubmissionFileDetailController', SubmissionFileDetailController);

    SubmissionFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubmissionFile'];

    function SubmissionFileDetailController($scope, $rootScope, $stateParams, previousState, entity, SubmissionFile) {
        var vm = this;

        vm.submissionFile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:submissionFileUpdate', function (event, result) {
            vm.submissionFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
