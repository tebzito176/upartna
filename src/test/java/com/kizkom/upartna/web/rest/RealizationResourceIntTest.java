package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.Realization;
import com.kizkom.upartna.repository.RealizationRepository;
import com.kizkom.upartna.service.RealizationService;
import com.kizkom.upartna.service.dto.RealizationDTO;
import com.kizkom.upartna.service.mapper.RealizationMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RealizationResource REST controller.
 *
 * @see RealizationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class RealizationResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REALIZATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REALIZATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    @Autowired
    private RealizationRepository realizationRepository;

    @Autowired
    private RealizationMapper realizationMapper;

    @Autowired
    private RealizationService realizationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restRealizationMockMvc;

    private Realization realization;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Realization createEntity() {
        Realization realization = new Realization()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .realizationDate(DEFAULT_REALIZATION_DATE)
            .location(DEFAULT_LOCATION);
        return realization;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RealizationResource realizationResource = new RealizationResource(realizationService);
        this.restRealizationMockMvc = MockMvcBuilders.standaloneSetup(realizationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        realizationRepository.deleteAll();
        realization = createEntity();
    }

    @Test
    public void createRealization() throws Exception {
        int databaseSizeBeforeCreate = realizationRepository.findAll().size();

        // Create the Realization
        RealizationDTO realizationDTO = realizationMapper.toDto(realization);
        restRealizationMockMvc.perform(post("/api/realizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Realization in the database
        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeCreate + 1);
        Realization testRealization = realizationList.get(realizationList.size() - 1);
        assertThat(testRealization.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testRealization.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRealization.getRealizationDate()).isEqualTo(DEFAULT_REALIZATION_DATE);
        assertThat(testRealization.getLocation()).isEqualTo(DEFAULT_LOCATION);
    }

    @Test
    public void createRealizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = realizationRepository.findAll().size();

        // Create the Realization with an existing ID
        realization.setId("existing_id");
        RealizationDTO realizationDTO = realizationMapper.toDto(realization);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRealizationMockMvc.perform(post("/api/realizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Realization in the database
        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = realizationRepository.findAll().size();
        // set the field null
        realization.setTitle(null);

        // Create the Realization, which fails.
        RealizationDTO realizationDTO = realizationMapper.toDto(realization);

        restRealizationMockMvc.perform(post("/api/realizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationDTO)))
            .andExpect(status().isBadRequest());

        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllRealizations() throws Exception {
        // Initialize the database
        realizationRepository.save(realization);

        // Get all the realizationList
        restRealizationMockMvc.perform(get("/api/realizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(realization.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].realizationDate").value(hasItem(DEFAULT_REALIZATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())));
    }

    @Test
    public void getRealization() throws Exception {
        // Initialize the database
        realizationRepository.save(realization);

        // Get the realization
        restRealizationMockMvc.perform(get("/api/realizations/{id}", realization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(realization.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.realizationDate").value(DEFAULT_REALIZATION_DATE.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()));
    }

    @Test
    public void getNonExistingRealization() throws Exception {
        // Get the realization
        restRealizationMockMvc.perform(get("/api/realizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateRealization() throws Exception {
        // Initialize the database
        realizationRepository.save(realization);
        int databaseSizeBeforeUpdate = realizationRepository.findAll().size();

        // Update the realization
        Realization updatedRealization = realizationRepository.findOne(realization.getId());
        updatedRealization
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .realizationDate(UPDATED_REALIZATION_DATE)
            .location(UPDATED_LOCATION);
        RealizationDTO realizationDTO = realizationMapper.toDto(updatedRealization);

        restRealizationMockMvc.perform(put("/api/realizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationDTO)))
            .andExpect(status().isOk());

        // Validate the Realization in the database
        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeUpdate);
        Realization testRealization = realizationList.get(realizationList.size() - 1);
        assertThat(testRealization.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testRealization.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRealization.getRealizationDate()).isEqualTo(UPDATED_REALIZATION_DATE);
        assertThat(testRealization.getLocation()).isEqualTo(UPDATED_LOCATION);
    }

    @Test
    public void updateNonExistingRealization() throws Exception {
        int databaseSizeBeforeUpdate = realizationRepository.findAll().size();

        // Create the Realization
        RealizationDTO realizationDTO = realizationMapper.toDto(realization);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRealizationMockMvc.perform(put("/api/realizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Realization in the database
        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteRealization() throws Exception {
        // Initialize the database
        realizationRepository.save(realization);
        int databaseSizeBeforeDelete = realizationRepository.findAll().size();

        // Get the realization
        restRealizationMockMvc.perform(delete("/api/realizations/{id}", realization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Realization> realizationList = realizationRepository.findAll();
        assertThat(realizationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Realization.class);
        Realization realization1 = new Realization();
        realization1.setId("id1");
        Realization realization2 = new Realization();
        realization2.setId(realization1.getId());
        assertThat(realization1).isEqualTo(realization2);
        realization2.setId("id2");
        assertThat(realization1).isNotEqualTo(realization2);
        realization1.setId(null);
        assertThat(realization1).isNotEqualTo(realization2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RealizationDTO.class);
        RealizationDTO realizationDTO1 = new RealizationDTO();
        realizationDTO1.setId("id1");
        RealizationDTO realizationDTO2 = new RealizationDTO();
        assertThat(realizationDTO1).isNotEqualTo(realizationDTO2);
        realizationDTO2.setId(realizationDTO1.getId());
        assertThat(realizationDTO1).isEqualTo(realizationDTO2);
        realizationDTO2.setId("id2");
        assertThat(realizationDTO1).isNotEqualTo(realizationDTO2);
        realizationDTO1.setId(null);
        assertThat(realizationDTO1).isNotEqualTo(realizationDTO2);
    }
}
