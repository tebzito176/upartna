package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.Speciality;
import com.kizkom.upartna.repository.SpecialityRepository;
import com.kizkom.upartna.service.SpecialityService;
import com.kizkom.upartna.service.dto.SpecialityDTO;
import com.kizkom.upartna.service.mapper.SpecialityMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SpecialityResource REST controller.
 *
 * @see SpecialityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class SpecialityResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private SpecialityRepository specialityRepository;

    @Autowired
    private SpecialityMapper specialityMapper;

    @Autowired
    private SpecialityService specialityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restSpecialityMockMvc;

    private Speciality speciality;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Speciality createEntity() {
        Speciality speciality = new Speciality()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION);
        return speciality;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpecialityResource specialityResource = new SpecialityResource(specialityService);
        this.restSpecialityMockMvc = MockMvcBuilders.standaloneSetup(specialityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        specialityRepository.deleteAll();
        speciality = createEntity();
    }

    @Test
    public void createSpeciality() throws Exception {
        int databaseSizeBeforeCreate = specialityRepository.findAll().size();

        // Create the Speciality
        SpecialityDTO specialityDTO = specialityMapper.toDto(speciality);
        restSpecialityMockMvc.perform(post("/api/specialities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specialityDTO)))
            .andExpect(status().isCreated());

        // Validate the Speciality in the database
        List<Speciality> specialityList = specialityRepository.findAll();
        assertThat(specialityList).hasSize(databaseSizeBeforeCreate + 1);
        Speciality testSpeciality = specialityList.get(specialityList.size() - 1);
        assertThat(testSpeciality.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSpeciality.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createSpecialityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = specialityRepository.findAll().size();

        // Create the Speciality with an existing ID
        speciality.setId("existing_id");
        SpecialityDTO specialityDTO = specialityMapper.toDto(speciality);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpecialityMockMvc.perform(post("/api/specialities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specialityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Speciality in the database
        List<Speciality> specialityList = specialityRepository.findAll();
        assertThat(specialityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllSpecialities() throws Exception {
        // Initialize the database
        specialityRepository.save(speciality);

        // Get all the specialityList
        restSpecialityMockMvc.perform(get("/api/specialities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(speciality.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void getSpeciality() throws Exception {
        // Initialize the database
        specialityRepository.save(speciality);

        // Get the speciality
        restSpecialityMockMvc.perform(get("/api/specialities/{id}", speciality.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(speciality.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingSpeciality() throws Exception {
        // Get the speciality
        restSpecialityMockMvc.perform(get("/api/specialities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSpeciality() throws Exception {
        // Initialize the database
        specialityRepository.save(speciality);
        int databaseSizeBeforeUpdate = specialityRepository.findAll().size();

        // Update the speciality
        Speciality updatedSpeciality = specialityRepository.findOne(speciality.getId());
        updatedSpeciality
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION);
        SpecialityDTO specialityDTO = specialityMapper.toDto(updatedSpeciality);

        restSpecialityMockMvc.perform(put("/api/specialities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specialityDTO)))
            .andExpect(status().isOk());

        // Validate the Speciality in the database
        List<Speciality> specialityList = specialityRepository.findAll();
        assertThat(specialityList).hasSize(databaseSizeBeforeUpdate);
        Speciality testSpeciality = specialityList.get(specialityList.size() - 1);
        assertThat(testSpeciality.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSpeciality.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void updateNonExistingSpeciality() throws Exception {
        int databaseSizeBeforeUpdate = specialityRepository.findAll().size();

        // Create the Speciality
        SpecialityDTO specialityDTO = specialityMapper.toDto(speciality);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSpecialityMockMvc.perform(put("/api/specialities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specialityDTO)))
            .andExpect(status().isCreated());

        // Validate the Speciality in the database
        List<Speciality> specialityList = specialityRepository.findAll();
        assertThat(specialityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteSpeciality() throws Exception {
        // Initialize the database
        specialityRepository.save(speciality);
        int databaseSizeBeforeDelete = specialityRepository.findAll().size();

        // Get the speciality
        restSpecialityMockMvc.perform(delete("/api/specialities/{id}", speciality.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Speciality> specialityList = specialityRepository.findAll();
        assertThat(specialityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Speciality.class);
        Speciality speciality1 = new Speciality();
        speciality1.setId("id1");
        Speciality speciality2 = new Speciality();
        speciality2.setId(speciality1.getId());
        assertThat(speciality1).isEqualTo(speciality2);
        speciality2.setId("id2");
        assertThat(speciality1).isNotEqualTo(speciality2);
        speciality1.setId(null);
        assertThat(speciality1).isNotEqualTo(speciality2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpecialityDTO.class);
        SpecialityDTO specialityDTO1 = new SpecialityDTO();
        specialityDTO1.setId("id1");
        SpecialityDTO specialityDTO2 = new SpecialityDTO();
        assertThat(specialityDTO1).isNotEqualTo(specialityDTO2);
        specialityDTO2.setId(specialityDTO1.getId());
        assertThat(specialityDTO1).isEqualTo(specialityDTO2);
        specialityDTO2.setId("id2");
        assertThat(specialityDTO1).isNotEqualTo(specialityDTO2);
        specialityDTO1.setId(null);
        assertThat(specialityDTO1).isNotEqualTo(specialityDTO2);
    }
}
