package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.SubmissionFile;
import com.kizkom.upartna.repository.SubmissionFileRepository;
import com.kizkom.upartna.service.SubmissionFileService;
import com.kizkom.upartna.service.dto.SubmissionFileDTO;
import com.kizkom.upartna.service.mapper.SubmissionFileMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubmissionFileResource REST controller.
 *
 * @see SubmissionFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class SubmissionFileResourceIntTest {

    @Autowired
    private SubmissionFileRepository submissionFileRepository;

    @Autowired
    private SubmissionFileMapper submissionFileMapper;

    @Autowired
    private SubmissionFileService submissionFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restSubmissionFileMockMvc;

    private SubmissionFile submissionFile;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubmissionFile createEntity() {
        SubmissionFile submissionFile = new SubmissionFile();
        return submissionFile;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubmissionFileResource submissionFileResource = new SubmissionFileResource(submissionFileService);
        this.restSubmissionFileMockMvc = MockMvcBuilders.standaloneSetup(submissionFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        submissionFileRepository.deleteAll();
        submissionFile = createEntity();
    }

    @Test
    public void createSubmissionFile() throws Exception {
        int databaseSizeBeforeCreate = submissionFileRepository.findAll().size();

        // Create the SubmissionFile
        SubmissionFileDTO submissionFileDTO = submissionFileMapper.toDto(submissionFile);
        restSubmissionFileMockMvc.perform(post("/api/submission-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(submissionFileDTO)))
            .andExpect(status().isCreated());

        // Validate the SubmissionFile in the database
        List<SubmissionFile> submissionFileList = submissionFileRepository.findAll();
        assertThat(submissionFileList).hasSize(databaseSizeBeforeCreate + 1);
        SubmissionFile testSubmissionFile = submissionFileList.get(submissionFileList.size() - 1);
    }

    @Test
    public void createSubmissionFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = submissionFileRepository.findAll().size();

        // Create the SubmissionFile with an existing ID
        submissionFile.setId("existing_id");
        SubmissionFileDTO submissionFileDTO = submissionFileMapper.toDto(submissionFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubmissionFileMockMvc.perform(post("/api/submission-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(submissionFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubmissionFile in the database
        List<SubmissionFile> submissionFileList = submissionFileRepository.findAll();
        assertThat(submissionFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllSubmissionFiles() throws Exception {
        // Initialize the database
        submissionFileRepository.save(submissionFile);

        // Get all the submissionFileList
        restSubmissionFileMockMvc.perform(get("/api/submission-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(submissionFile.getId())));
    }

    @Test
    public void getSubmissionFile() throws Exception {
        // Initialize the database
        submissionFileRepository.save(submissionFile);

        // Get the submissionFile
        restSubmissionFileMockMvc.perform(get("/api/submission-files/{id}", submissionFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(submissionFile.getId()));
    }

    @Test
    public void getNonExistingSubmissionFile() throws Exception {
        // Get the submissionFile
        restSubmissionFileMockMvc.perform(get("/api/submission-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSubmissionFile() throws Exception {
        // Initialize the database
        submissionFileRepository.save(submissionFile);
        int databaseSizeBeforeUpdate = submissionFileRepository.findAll().size();

        // Update the submissionFile
        SubmissionFile updatedSubmissionFile = submissionFileRepository.findOne(submissionFile.getId());
        SubmissionFileDTO submissionFileDTO = submissionFileMapper.toDto(updatedSubmissionFile);

        restSubmissionFileMockMvc.perform(put("/api/submission-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(submissionFileDTO)))
            .andExpect(status().isOk());

        // Validate the SubmissionFile in the database
        List<SubmissionFile> submissionFileList = submissionFileRepository.findAll();
        assertThat(submissionFileList).hasSize(databaseSizeBeforeUpdate);
        SubmissionFile testSubmissionFile = submissionFileList.get(submissionFileList.size() - 1);
    }

    @Test
    public void updateNonExistingSubmissionFile() throws Exception {
        int databaseSizeBeforeUpdate = submissionFileRepository.findAll().size();

        // Create the SubmissionFile
        SubmissionFileDTO submissionFileDTO = submissionFileMapper.toDto(submissionFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubmissionFileMockMvc.perform(put("/api/submission-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(submissionFileDTO)))
            .andExpect(status().isCreated());

        // Validate the SubmissionFile in the database
        List<SubmissionFile> submissionFileList = submissionFileRepository.findAll();
        assertThat(submissionFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteSubmissionFile() throws Exception {
        // Initialize the database
        submissionFileRepository.save(submissionFile);
        int databaseSizeBeforeDelete = submissionFileRepository.findAll().size();

        // Get the submissionFile
        restSubmissionFileMockMvc.perform(delete("/api/submission-files/{id}", submissionFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubmissionFile> submissionFileList = submissionFileRepository.findAll();
        assertThat(submissionFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubmissionFile.class);
        SubmissionFile submissionFile1 = new SubmissionFile();
        submissionFile1.setId("id1");
        SubmissionFile submissionFile2 = new SubmissionFile();
        submissionFile2.setId(submissionFile1.getId());
        assertThat(submissionFile1).isEqualTo(submissionFile2);
        submissionFile2.setId("id2");
        assertThat(submissionFile1).isNotEqualTo(submissionFile2);
        submissionFile1.setId(null);
        assertThat(submissionFile1).isNotEqualTo(submissionFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubmissionFileDTO.class);
        SubmissionFileDTO submissionFileDTO1 = new SubmissionFileDTO();
        submissionFileDTO1.setId("id1");
        SubmissionFileDTO submissionFileDTO2 = new SubmissionFileDTO();
        assertThat(submissionFileDTO1).isNotEqualTo(submissionFileDTO2);
        submissionFileDTO2.setId(submissionFileDTO1.getId());
        assertThat(submissionFileDTO1).isEqualTo(submissionFileDTO2);
        submissionFileDTO2.setId("id2");
        assertThat(submissionFileDTO1).isNotEqualTo(submissionFileDTO2);
        submissionFileDTO1.setId(null);
        assertThat(submissionFileDTO1).isNotEqualTo(submissionFileDTO2);
    }
}
